
/*Deklarasi variavel*/
var probCross = 0.7;
var probMutate = 0.02;
var popSize = 100;
var maxGen = 20;

var temp_syntax = [];

/*probabilitas dipilihnya function dalam mengisi node tree*/
var probFunction = 0.5;

/*probabilitas dipilihnya nilai konstant, dibanding dengan variabel*/
var probConstant = 0.5;

var listFunction = ["+","-","*"];

var listConstant = [1,2,3,4,5,6,7,8,9];
var listVariable = ['x','y'];

var counter_left = 0;
var counter_right = 0;

var maxDeep = 5;

/*minimum batas firness untuk data diambil sebagai calon parent*/
var minFitness = 0.001; 

var trainingData = [
	{'x' : 1, 'y' : 2, 'z': 1},
	{'x' : 2, 'y' : 2, 'z': 12},
	{'x' : 3, 'y' : 3, 'z': 5},
	{'x' : 4, 'y' : 5, 'z': 46},
	{'x' : 5, 'y' : 3, 'z': 18},
	{'x' : 6, 'y' : 5, 'z': 56},
	{'x' : 7, 'y' : 4, 'z': 35},
	{'x' : 8, 'y' : 6, 'z': 84},
	{'x' : 9, 'y' : 3, 'z': 56},
	{'x' : 10,'y' : 8, 'z': 148},
];



function evolition(){
	var population = [];
	var goodParent = [];
	var bestIndividual = null;
	for(i=0;i<popSize;i++){
		population[i] = generateRandom();
		if(parseFloat(1/(1+population[i].error)) > minFitness){
			goodParent.push(population[i]);
			//console.log(population[i]);
		}
		if(bestIndividual == null || population[i].error < bestIndividual.error ){
			bestIndividual = population[i];
		}
	}
	//console.log(goodParent.length);
	//console.log(bestIndividual);
	//console.log(crossover(goodParent[1], goodParent[2]));

	console.log(mutasi(goodParent[0]));

}


function crossover(gen1, gen2){
	console.log(gen1.tree.root);
	console.log(gen2.tree.root);
	var level = Math.floor(Math.random() * maxDeep);
	//console.log(level);
	console.log(gen1.postfix);
	console.log(gen2.postfix);
	var current = [gen1.tree.root,gen2.tree.root];
	//console.log(current);

	var path_swap = [[],[]];
	var path_swap_obj = ["",""];
	for(var i = 0; i <= level; i++){
		if( (current[0].left == null & current[0].right == null) || (current[1].left == null & current[1].right == null) ){
			break;
		}else{
			left = (Math.random() >0.5);
			if(left){
				current[0] = current[0].left;
				path_swap[0].push('left');
				path_swap_obj[0] += path_swap_obj[0] == "" ? ("left") : (".left");
			}else{
				current[0] = current[0].right;
				path_swap[0].push('right');
				path_swap_obj[0] += path_swap_obj[0] == "" ? ("right") : (".right");
			}
			left = (Math.random() >0.5);
			if(left){
				current[1] = current[1].left;
				path_swap[1].push('left');
				path_swap_obj[1] += path_swap_obj[1] == "" ? ("left") : (".left");
			}else{
				current[1] = current[1].right;
				path_swap[1].push('right');
				path_swap_obj[1] += path_swap_obj[1] == "" ? ("right") : (".right");
			}
		}
	}
	//console.log(path_swap);
	console.log(path_swap_obj);

	//console.log(gen2.tree.root);

	//console.log(Object.byStringe(gen1.tree.root, path_swap[0]));
	//console.log(Object.byStringe(gen2.tree.root, path_swap[1]));
	//console.log(gen1.tree.root)

	switch(path_swap[0].length){
		case 1 : 
			// console.log("SIJI");
			// console.log('part1');
			// console.log(gen1.tree.root[path_swap[0][0]]);
			var temp = $.extend(true,{},gen1.tree.root[path_swap[0][0]]);
			// console.log('should be part1');
			// console.log(temp);
			// console.log('part2');
			// console.log(gen2.tree.root[path_swap[1][0]]);
			gen1.tree.root[path_swap[0][0]] = $.extend(true,{},gen2.tree.root[path_swap[1][0]]);
			// console.log('should be part2 too');
			// console.log(gen1.tree.root[path_swap[0][0]]);
			gen2.tree.root[path_swap[1][0]] = $.extend(true,{},temp);
			// console.log('should be part1 too');
			// console.log(gen2.tree.root[path_swap[1][0]]);
			break;
		case 2 :
			var temp = $.extend(true,{},gen1.tree.root[path_swap[0][0]][path_swap[0][1]]);
			gen1.tree.root[path_swap[0][0]][path_swap[0][1]] = $.extend(true,{},gen2.tree.root[path_swap[1][0]][path_swap[1][1]]);
			gen2.tree.root[path_swap[1][0]][path_swap[1][1]] = $.extend(true,{},temp);
			break;
		case 3 :
			var temp = $.extend(true,{},gen1.tree.root[path_swap[0][0]][path_swap[0][1]][path_swap[0][2]]);
			gen1.tree.root[path_swap[0][0]][path_swap[0][1]][path_swap[0][2]] = $.extend(true,{},gen2.tree.root[path_swap[1][0]][path_swap[1][1]][path_swap[1][2]]);
			gen2.tree.root[path_swap[1][0]][path_swap[1][1]][path_swap[1][2]] = $.extend(true,{},temp);
			break;
		case 4 :
			var temp = $.extend(true,{},gen1.tree.root[path_swap[0][0]][path_swap[0][1]][path_swap[0][2]][path_swap[0][3]]);
			gen1.tree.root[path_swap[0][0]][path_swap[0][1]][path_swap[0][2]][path_swap[0][3]] = $.extend(true,{},gen2.tree.root[path_swap[1][0]][path_swap[1][1]][path_swap[1][2]][path_swap[1][3]]);
			gen2.tree.root[path_swap[1][0]][path_swap[1][1]][path_swap[1][2]][path_swap[1][3]] = $.extend(true,{},temp);
			break;
	}
	console.log('after swap');
	console.log('gen1');
	console.log(gen1.tree.root);
	console.log(getPostfix(gen1.tree.root,[]));

	console.log('gen2');
	console.log(gen2.tree.root);
	console.log(getPostfix(gen2.tree.root,[]));

	gen1.postfix = getPostfix(gen1.tree.root,[]);
	gen1.error = calculateError(gen1.postfix);
	gen2.postfix = getPostfix(gen2.tree.root,[]);
	gen2.error = calculateError(gen2.postfix);

	return [gen1,gen2];
	//crossTree(gen1.tree, path_swap, Object.byStringe(gen2.tree.root, path_swap[1]) );
}

function mutasi(gen){
	console.log(gen.tree.root);
	console.log(gen.postfix);
	var level = Math.floor(Math.random() * maxDeep);
	var current = gen.tree.root;
	var path = [];
	for(var i = 0; i <= level; i++){
		if(current.left == null & current.right == null){
			break;
		}else{
			left = (Math.random() >0.5);
			if(left){
				current = current.left;
				path.push("left");
			}else{
				current = current.right;
				path.push("right");
			}
		}
	}

	// if( !forceTerminal && (Math.random() < probFunction) ){
	// 	//funtion dipilih
	// 	var nd = new Node(listFunction[Math.floor(Math.random() * listFunction.length)] ,null,null,deep);
	// }else{
	// 	//terminal dipilih
	// 	//menentukan apakah memilih angka konstant atau variable
	// 	if(Math.random() < probConstant){
	// 		//konstant dipilih
	// 		var nd = new Node(listConstant[Math.floor(Math.random() * listConstant.length)] ,null,null,deep);
	// 	}else{
	// 		//variable dipilih
	// 		var nd = new Node(listVariable[Math.floor(Math.random() * listVariable.length)] ,null,null,deep);
	// 	}
	// }

	console.log(path);
	console.log(current.data);
	if(listFunction.indexOf(current.data) != -1){
		replacement = current.data;
		while(replacement == current.data){
			replacement = listFunction[Math.floor(Math.random() * listFunction.length)];
		}
	}else{
		console.log('terminal');
		replacement = current.data;
		while(replacement == current.data){
			if(Math.random() < probConstant){
				//konstant dipilih
				replacement = listConstant[Math.floor(Math.random() * listConstant.length)];
			}else{
				//variable dipilih
				replacement = listVariable[Math.floor(Math.random() * listVariable.length)];
			}
		}
	}
	console.log(replacement);

	switch(path.length){
		case 1 : 
			gen.tree.root[path[0]].data = replacement;
			break;
		case 2 :
			gen.tree.root[path[0]][path[1]].data = replacement;
			break;
		case 3 :
			gen.tree.root[path[0]][path[1]][path[2]].data = replacement;
			break;
		case 4 :
			gen.tree.root[path[0]][path[1]][path[2]][path[3]].data = replacement;
			break;
	}
	//console.log(current);
	// if(listFunction.indexOf(current.data) != -1){
	// 	console.log('function');
	// 	replacement = current.data;
	// 	while(replacement == current.data){
	// 		replacement = listFunction[Math.floor(Math.random() * listFunction.length)];
	// 	}
	// 	current.data = replacement;
		
	// }else{
	// 	console.log('terminal');
	// 	replacement = current.data;
	// 	while(replacement == current.data){
	// 		if(Math.random() < probConstant){
	// 			//konstant dipilih
	// 			replacement = listConstant[Math.floor(Math.random() * listConstant.length)];
	// 		}else{
	// 			//variable dipilih
	// 			replacement = listVariable[Math.floor(Math.random() * listVariable.length)];
	// 		}
	// 	}
	// 	current.data = replacement;
	// }
	//console.log(current);
	gen.postfix = getPostfix(gen.tree.root,[]);
	gen.error = calculateError(gen.postfix);
	console.log(gen.postfix);
	console.log(gen.tree.root);
}

// function swapNode(node, path){
// 	var newNode = [null,null];
// 	temp = null;
// 	for (var i = 0, n = a.length; i < n; ++i) {
// 		if(i == 0){
// 			var newNode[0] = new Tree();
// 			//inisialiasi root dengan random function
// 			var rNode = new Node( ,null,null,1);
// 			newNode[0].root = rNode;
// 		}
// 	}
	
// 	console.log(newNode[0]);
// }

function crossTree(tree,path,newPart){
	console.log('crossTree');
	console.log(tree);
	console.log(path);
	console.log(newPart);

	var nTree = new Tree();

	//inisialiasi root dengan random function
	nTree.root = new Node(tree.root.data ,null,null,1);

	postfix = getPostfix(tree.root,[]);
	error = calculateError(postfix);


	for(var i = 0; i < path.length; ++i){
		console.log(path[i]);
		if(path[i] == "left"){

		}else{

		}
	}

	console.log(nTree);

}


Object.byStringe = function(o, a) {
    // s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    // s = s.replace(/^\./, '');           // strip a leading dot
    //var a = s.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
}

Object.byString = function(o, s) {
    s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    s = s.replace(/^\./, '');           // strip a leading dot
    var a = s.split('.');
    for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
            o = o[k];
        } else {
            return;
        }
    }
    return o;
}

function getPropertyn(obj, parts) {
    if (Array.isArray(parts)) {
        var last = parts.pop(),
        l = parts.length,
        i = 1,
        current = parts[0];

        while((obj = obj[current]) && i < l) {
            current = parts[i];
            i++;
        }

        if(obj) {
            return obj[last];
        }
    } else {
        throw 'parts is not valid array';
    }
}

function getProperty(obj, prop) {
    var parts = prop.split('.');

    if (Array.isArray(parts)) {
        var last = parts.pop(),
        l = parts.length,
        i = 1,
        current = parts[0];

        while((obj = obj[current]) && i < l) {
            current = parts[i];
            i++;
        }

        if(obj) {
            return obj[last];
        }
    } else {
        throw 'parts is not valid array';
    }
}


function generateRandom(){
	var tree = new Tree();

	//inisialiasi root dengan random function
	var rNode = new Node(listFunction[Math.floor(Math.random() * listFunction.length)] ,null,null,1);
	tree.root = rNode;
	tree.root = generateChild(tree.root);

	postfix = getPostfix(tree.root,[]);
	error = calculateError(postfix);

	return {'tree':tree,'postfix':postfix, 'error' : error};
}

function getInfix(node,infix){
    if (!(node == null)) { 
        getInfix(node.left,infix); 
        //console.log(node.show() + " ");
        infix.push(node.show());
        getInfix(node.right,infix); 
    } 
    return infix;
}

function getPostfix(node,postfix){
    if (!(node == null)) { 
        getPostfix(node.left,postfix); 
        getPostfix(node.right,postfix); 

        postfix.push(node.show());
    } 

    return postfix;
}

function calculatePostfix(postfix, x, y){
	var postfixStack = [];
	postfix.forEach( function(current) {
	    if (isOperator(current) ) {
	    	var calculation = compute( postfixStack.pop(), symbolToOperator(current), postfixStack.pop() );
	        postfixStack.push(calculation);
	    }
	    else {
	    	if(current == 'x'){
	    		postfixStack.push(x);
	    	}else if(current == 'y'){
	    		postfixStack.push(y);
	    	}else{
	    		postfixStack.push(parseInt(current));
	    	}
	        
	    }   
	});

	return postfixStack[0];
}

function calculateError(postfix){
	var sum_error = 0;
	trainingData.forEach(function(current){
		postfix_calc = calculatePostfix(postfix,current.x,current.y);
		sum_error += postfix_calc > current.z ? (postfix_calc-current.z) : (current.z - postfix_calc);
	});

	return sum_error;
}

function calculateFitness(postfix){
	var sum_error = 0;
	trainingData.forEach(function(current){
		postfix_calc = calculatePostfix(postfix,current.x,current.y);
		console.log("#" +current.z +" VS " +postfix_calc);
		console.log("#err = " +( postfix_calc > current.z ? (postfix_calc-current.z) : (current.z - postfix_calc)) );
		sum_error += postfix_calc > current.z ? (postfix_calc-current.z) : (current.z - postfix_calc);
	});

	return {'error':sum_error, 'fitness':(1/(1+sum_error))};
}

function calculateFunction(parent,x,y,result){
	if( ( parent.left.data == null ) || (listFunction.indexOf(parent.left.data) == -1)  && ( (parent.right.data==null) || (listFunction.indexOf(parent.right.data) == -1))){
		if(parent.data == "*"){
			
		}else if(parent.data == "+" ){

		}else if(parent.data == "-"){

		}
	}else{
		if(listFunction.indexOf(parent.left.show()) != -1){
			calculateFunction(parent.left,x,y,result);
		}
		if(listFunction.indexOf(parent.right.show()) != -1){
			calculateFunction(parent.left,x,y,result);
		}

	}
}

function generateChild(parent){
	if(listFunction.indexOf(parent.data) != -1){
		forceTerminal = (parent.deep+1) == parseInt(maxDeep);
		parent.left = generateRandomNode(parent.deep+1, forceTerminal );
		parent.right = generateRandomNode(parent.deep+1, forceTerminal );

		parent.left = generateChild(parent.left);
		parent.right = generateChild(parent.right);
	}
	return parent;
}

function generateRandomNode(deep,forceTerminal){
	//menentukan mengisi node dengan terminal atau function, dengan probalilitas function
	if( !forceTerminal && (Math.random() < probFunction) ){
		//funtion dipilih
		var nd = new Node(listFunction[Math.floor(Math.random() * listFunction.length)] ,null,null,deep);
	}else{
		//terminal dipilih
		//menentukan apakah memilih angka konstant atau variable
		if(Math.random() < probConstant){
			//konstant dipilih
			var nd = new Node(listConstant[Math.floor(Math.random() * listConstant.length)] ,null,null,deep);
		}else{
			//variable dipilih
			var nd = new Node(listVariable[Math.floor(Math.random() * listVariable.length)] ,null,null,deep);
		}
	}

	return nd;
}


/*TREE*/
	function Node(data, left, right, deep) { 
		this.show = show; 
	    this.data = data;
	    this.left = left; 
	    this.right = right; 
	    this.deep = deep;
	    
	} 
	function show() { 
	    return this.data; 
	} 

	function Tree() { 
	    this.root = null; 
	    //this.insert = insert; 
	    //this.inOrder = inOrder; 
	    //this.preOrder = preOrder;
	   	//this.postOrder = postOrder;
	    //this.getMin = getMin;
	    //this.getMax = getMax;
	    //this.find = find;
	    //this.remove = remove;
	    //this.removeNode = removeNode;
	}

	function insert(data) { 
	    var n= new Node(data, null, null); 
	    if (this.root == null){ 
	        this.root = n; 
	    } else { 
	        var current = this.root; 
	        var parent; 
	        while (true){ 
	            parent = current; 
	            if (data < current.data) { 
	                current = current.left; 
	                if (current == null){ 
	                    parent.left = n; 
	                    break; 
	                } 
	            } else { 
	                current = current.right; 
	                if (current == null){ 
	                    parent.right = n; 
	                    break; 
	                } 
	            } 
	        } 
	    } 
	}

	function inOrder(node) { 
	    if (!(node == null)) { 
	        inOrder(node.left); 
	        console.log(node.show() + " ");
	        temp_syntax.push(node.show());
	        //here = node.show;
	        inOrder(node.right); 
	    } 
	    //return here;
	}

	function preOrder(node) { 
	    if (!(node == null)) { 
	        console.log(node.show() + " "); 
	        preOrder(node.left); 
	        preOrder(node.right); 
	    } 
	} 

	function postOrder(node) { 
	    if (!(node == null)) { 
	        postOrder(node.left); 
	        postOrder(node.right); 
	        console.log(node.show() + " "); 
	    } 
	} 

	function getMin() { 
	    var current = this.root; 
	    while (!(current.left == null)) { 
	        current = current.left; 
	    } 
	    return current.data; 
	}

	function getMax() { 
	    var current = this.root; 
	    while (!(current.right == null)) { 
	        current = current.right; 
	    } 
	    return current.data; 
	} 

	function find(data) { 
	    var current = this.root; 
	    while (current.data != data) { 
	        if (data < current.data) { 
	            current = current.left; 
	        } else { 
	            current = current.right; 
	        } 
	        if (current == null){ 
	            return null; 
	        } 
	    } 
	    return current; 
	}

	function remove(data) { 
	    root = removeNode(this.root, data); 
	}

	function removeNode(node, data) { 
	    if (node == null){ 
	        return null; 
	    } 
	    if (data == node.data) { 
	        // node has no children 
	        if (node.left == null && node.right == null){ 
	            return null; 
	        } 
	        // node has no left child 
	        if (node.left == null){ 
	            return node.right; 
	        } 
	        // node has no right child 
	        if (node.right == null){ 
	            return node.left; 
	        } 
	        // node has two children 
	        var tempNode = getSmallest(node.right); 
	        node.data = tempNode.data; 
	        node.right = removeNode(node.right, tempNode.data); 
	        return node; 
	    } else if (data < node.data) { 
	        node.left = removeNode(node.left, data); 
	        return node; 
	    } else { 
	        node.right = removeNode(node.right, data); 
	        return node; 
	    } 
	} 
/*END TREE*/

/*POSTFIX CALCULATION*/
	function isOperator(toCheck) {
	    switch (toCheck) {
	        case '+':
	        case '-':
	        case '*':
	        case '/':
	        case '%':
	            return true;
	        default:
	            return false;
	    }
	}

	function compute(a, operator, b) {
	    return operator(a,b); 
	}

	function symbolToOperator(symbol) {
	    switch (symbol) {
	        case '+': return plus;
	        case '-': return minus;
	        case '*': return multiply;
	        case '/': return divide;
	        case '%': return modulo;
	    }
	}

	function plus(a,b) { return a + b; } 
	function minus(a,b) { return a - b; }
	function multiply(a,b) { return a * b; }
	function divide(a,b) { return a / b; }
	function modulo(a,b) { return a % b; }
/*POSTFIX CALCULATION*/




